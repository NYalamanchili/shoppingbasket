package com.cart.shopping.service

import com.cart.shopping.datamodel._

import scala.collection.mutable.ListBuffer

object shoppingCart {
  val currentOffers: List[Offer] = List(NoOffer, BuyOneGetOneFreeOffer, BuyTwoGetThreeOffer)

  def calculatePayment(items: List[ShoppingItem]): BigDecimal = {
    val closedGroups: ListBuffer[Group] = ListBuffer.empty[Group]
    val openGroups: Map[String, Group] = Map(Apple().name -> BuyOneGetOneFreeGroup(),
       Orange().name -> BuyTwoGetThreeGroup(), "" -> NoDiscountGroup())
    items.foreach(item => buildGroups(item, openGroups, closedGroups))
    (closedGroups.toList++openGroups.values.toList).map(group => group.cost).sum
  }

  def buildGroups(item: ShoppingItem, currentWorkingGroups: Map[String, Group], closed: ListBuffer[Group]) = {
    val group: Option[Group] = addItem(item, currentWorkingGroups)
    if (group.nonEmpty) {
      closed += ClonedGroup(group.get)
      group.get.costItems.clear()
      group.get.freeItems.clear()
    }
  }

  def addItem(item: ShoppingItem, currentWorkingGroups: Map[String, Group]): Option[Group] = {
    val opt: Option[Group] = currentWorkingGroups.get(item.name)
    if (opt.nonEmpty) {
      opt.get.add(item)
      if (opt.get.full) {
        return opt
      }
    }
    Option.empty[Group]
  }
}
