package com.cart.shopping.datamodel

trait ShoppingItem {
  val name = ""
  val itemType = ""
  def cost : BigDecimal = 0
}

case class Apple(override val name:String = "Apple", override val cost: BigDecimal= 60, override val itemType:String = "Fruit") extends ShoppingItem
case class Orange(override val name:String = "Orange", override val cost: BigDecimal= 20, override val itemType:String = "Fruit") extends ShoppingItem

