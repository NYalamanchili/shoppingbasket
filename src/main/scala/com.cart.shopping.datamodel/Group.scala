package com.cart.shopping.datamodel

import scala.collection.mutable.ListBuffer

trait Group {
  val offer: Offer = NoOffer
  val costItems: ListBuffer[ShoppingItem] = ListBuffer.empty[ShoppingItem]
  val freeItems: ListBuffer[ShoppingItem] = ListBuffer.empty[ShoppingItem]

  def cost : BigDecimal = {
     costItems.toList.map(each => each.cost).sum - offer.cashBackAmount
  }

  def full : Boolean = {
    costItems.size == offer.payableItemCount && freeItems.size == offer.freeItemCount
  }


  def add(item: ShoppingItem): Boolean = {
    if(costItems.size < offer.payableItemCount) {
      costItems += item
    } else if ( freeItems.size < offer.freeItemCount) {
      freeItems += item
    } else {
      return false
    }
    true
  }
}

case class ClonedGroup (group :Group) extends Group {
    this.costItems ++=group.costItems
    this.freeItems ++=group.freeItems
}

case class NoDiscountGroup() extends Group
case class BuyOneGetOneFreeGroup(override val offer: Offer = BuyOneGetOneFreeOffer) extends Group
case class BuyTwoGetThreeGroup(override val offer:Offer= BuyTwoGetThreeOffer) extends Group