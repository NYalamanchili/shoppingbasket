package com.cart.shopping.datamodel

trait Offer {
  val payableItemCount: Int = Integer.MAX_VALUE
  val freeItemCount : Int = 0
  def cashBackAmount: BigDecimal = 0.0
  def fixedCostAmount: BigDecimal = 0.0
}

case object NoOffer extends Offer
case object BuyOneGetOneFreeOffer extends Offer {override val payableItemCount = 1; override  val freeItemCount =1}
case object BuyTwoGetThreeOffer extends Offer{override val payableItemCount = 2; override  val freeItemCount =1}